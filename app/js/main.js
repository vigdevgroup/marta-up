$(function () {

    $('.header__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        infinite: true,
        vertical: true,
        verticalSwiping: true,
        autoplay: true,
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    vertical: false,
                    verticalSwiping: false,
                }
            }
        ]
    });

    $('.mobile-menu').on('click', function () {
        $('.header .menu').addClass('show');
        setTimeout(() => {
            document.addEventListener('click', closeMenu)
        }, 0)
    });

    $('.header .menu').on('click', '.close', function() {
        $('.header .menu').removeClass('show');
        document.removeEventListener('click', closeMenu)
    })

    function closeMenu(event) {
        if (event.target.closest('.menu') && event.target.closest('.header')) return

        $('.header .menu').removeClass('show');
        document.removeEventListener('click', closeMenu)
    }


});